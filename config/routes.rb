Rails.application.routes.draw do

  # get 'listings/:id/pay', to: 'listings#pay'
  # post 'listings/:id/charge', to: 'listings#charge'
  resources :listings do
    member do
      get 'pay'
      post 'charge'
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
